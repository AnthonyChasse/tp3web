"""Contient la classe config"""


class Config:
    """Contient les informations nécessairement pour l'application flask"""
    DB_SERVEUR = "localhost:3308"
    DB_NAME = "tp3web"
    DB_USERNAME = "root"
    DB_PASSWORD = ""
    DEBUG = True
    SESSION_COOKIE_SECURE = True
    BABEL_DEFAULT_LOCALE = "fr_CA"

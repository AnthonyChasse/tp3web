import datetime

from bd import bd


class Cas(bd.Model):
    __tablename__ = "cas"
    id = bd.Column(bd.Integer, primary_key=True)
    date = bd.Column(bd.Date, nullable=False, default=datetime.datetime.now())
    prenom = bd.Column(bd.String(45), unique=True, nullable=False)
    nom = bd.Column(bd.String(45), unique=True, nullable=False)
    region_id = bd.Column(bd.Integer(), bd.ForeignKey('regions.id'), nullable=False)
    compte_id = bd.Column(bd.Integer(), bd.ForeignKey('comptes.id'), nullable=False)
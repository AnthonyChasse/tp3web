from bd import bd


class Region(bd.Model):
    __tablename__ = "regions"
    id = bd.Column(bd.Integer, primary_key=True)
    numero_region = bd.Column(bd.String(2), unique=True, nullable=False)
    nom = bd.Column(bd.String(80), unique=True, nullable=False)

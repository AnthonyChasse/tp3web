from bd import bd


class Compte(bd.Model):
    __tablename__ = "comptes"
    id = bd.Column(bd.Integer, primary_key=True)
    compte = bd.Column(bd.String(45), unique=True, nullable=False)
    admin = bd.Column(bd.Integer(), unique=True, nullable=False)
    password = bd.Column(bd.String(64), nullable=False)

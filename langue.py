"""Contient la fonction nécessaire pour le changement de langue"""
from flask import session


def get_locale():
    """Met la langue approprié dans la session"""
    if session.get("langue") is None:
        return "fr_CA"
    return session["langue"]

"""S'occupe d'importer et de créer l'application. Gère aussi les exceptions."""

from flask import Flask, render_template, session, request, redirect
from blueprints.comptes.gestion_comptes import comptes_routes
from blueprints.cas.gestion_cas import cas_routes
from bd import bd
import config


def creer_app():
    """Créer l'application et sqlalchemy à partir de config.py"""
    app_flask = Flask(__name__)
    app_flask.config.from_object(config.Config)
    app_flask.register_blueprint(comptes_routes)
    app_flask.config['SECRET_KEY'] = 'dame da ne dame yo dame na no yo'
    app_flask.register_blueprint(cas_routes)
    compte = f'{app_flask.config["DB_USERNAME"]}:{app_flask.config["DB_PASSWORD"]}'
    serveur_bd = f'{app_flask.config["DB_SERVEUR"]}/{app_flask.config["DB_NAME"]}'
    app_flask.config['SQLALCHEMY_DATABASE_URI'] = f"mysql://{compte}@{serveur_bd}"
    bd.init_app(app_flask)
    return app_flask


app = creer_app()


@app.route("/lang_change", methods=["POST"])
def lang_change():
    """Change la région"""
    session["langue"] = request.form["langue"]
    return redirect("/")


@app.errorhandler(401)
def page401(args):
    """Gère l'exception 401"""
    return render_template("page401.html", e=args), 401


@app.errorhandler(403)
def page403(args):
    """Gère l'exception 403"""
    return render_template("page403.html", e=args), 403


if __name__ == "__main__":
    app.run(debug=app.config["DEBUG"])

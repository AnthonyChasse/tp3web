"""Gère les routes relatant au cas"""
from flask import Blueprint, render_template, request, session
from babel import dates, numbers
from werkzeug.exceptions import abort
from langue import get_locale
from models.region import Region
from models.cas import Cas
from bd import bd

cas_routes = Blueprint('cas', __name__, template_folder='templates')


@cas_routes.route("/")
def accueil():
    """Affiche la page des cas par régions"""
    regions = Region.query.all()
    nb_cas = {}
    for region in regions:
        nb_cas[region.nom] = numbers.format_decimal(
            len(Cas.query.filter_by(region_id=region.id).all()),
            locale=get_locale())
    return render_template('index.html', regions=Region.query.all(), nb_cas=nb_cas)


@cas_routes.route("/saisie_cas")
def saisie_cas():
    """Affiche le formulaire d'ajout de cas"""
    if session.get("connected") != 1:
        session["redirect"] = "saisie_cas"
        abort(401)
    return render_template('saisie.html', regions=Region.query.all(), cas=None, erreurs=None)


@cas_routes.route("/ajout_cas", methods=["POST"])
def ajout_cas():
    """Permet d'ajouter un cas dans la base de données"""
    if session.get("connected") != 1:
        abort(401)
    erreurs = {}
    if Region.query.get(request.form["region"]) is None:
        erreurs["region"] = "Vous devez choisir une région valide."
    if len(request.form["nom"]) == 0:
        erreurs["nom"] = "Vous devez écrire un nom."
    if len(request.form["prenom"]) == 0:
        erreurs["prenom"] = "Vous devez écrire un prénom."
    if len(erreurs) != 0:
        return render_template("saisie.html", regions=Region.query.all(),
                               erreurs=erreurs, cas=request.form)
    cas = Cas(prenom=request.form["prenom"], nom=request.form["nom"],
              region_id=request.form["region"],
              compte_id=session["compte_id"])
    bd.session.add(cas)
    bd.session.commit()
    return render_template("saisie_reussite.html")


@cas_routes.route("/cas")
def liste_cas(deleted="false"):
    """Affiche tout les cas individuellement"""
    if session.get("connected") != 1 and session.get("admin") != 1:
        session["redirect"] = "cas"
        abort(401)
    dico_dates = {}
    for cas in Cas.query.all():
        dico_dates[cas.id] = dates.format_date(cas.date, locale=get_locale())
    return render_template("liste_admin.html", liste_cas=Cas.query.all(),
                           regions=Region.query.all(),
                           dates=dico_dates, deleted=deleted)


@cas_routes.route("/supprimer/<int:cas_id>")
def suppression(cas_id):
    """Permet de supprimer un cas"""
    if session.get("admin") != 1:
        abort(403)
    Cas.query.filter(Cas.id == cas_id).delete()
    bd.session.commit()
    return liste_cas(deleted="true")

"""Gère les routes relatant au comptes"""
import hashlib
from flask import Blueprint, render_template, request, session
from werkzeug.utils import redirect
from models.compte import Compte
from bd import bd

comptes_routes = Blueprint('comptes', __name__, template_folder='templates')


@comptes_routes.route('/inscription')
def inscription():
    """Affiche la page d'inscription"""
    return render_template('inscription.html', erreurs=None)


@comptes_routes.route('/add_compte', methods=["POST"])
def add_compte():
    """Permet d'ajouter un compte à la base de données"""
    erreurs = {}
    if len(Compte.query.filter_by(compte=request.form["nom"]).all()) != 0:
        erreurs["nom"] = "Ce nom est pris. "
    if len(request.form["nom"]) == 0:
        erreurs["nom"] += "Veuillez enter un nom."
    if len(request.form["motdepasse"]) == 0:
        erreurs["motdepasse"] = "Veuillez enter un mot de passe."
    if len(erreurs) != 0:
        return render_template('inscription.html', erreurs=erreurs)
    motdepasse = hashlib.sha256(request.form["motdepasse"].encode()).hexdigest()
    admin = 0
    if request.form.get("admin") == "1":
        admin = 1
    compte = Compte(compte=request.form["nom"], password=motdepasse, admin=admin)
    bd.session.add(compte)
    bd.session.commit()
    return render_template("connection.html", erreurs=None)


@comptes_routes.route('/connection')
def connection():
    """Affiche la page de connexion"""
    return render_template("connection.html", erreurs=None)


@comptes_routes.route('/connection_a_un_compte', methods=["POST"])
def conn_a_un_compte():
    """Permet de se connecter à un compte"""
    erreurs = {}
    if Compte.query.filter_by(compte=request.form["nom"]).first() is None or\
            Compte.query.filter_by(
                password=hashlib.sha256(
                    request.form["motdepasse"].encode()).hexdigest()).first()\
            is None:
        erreurs["nonexistant"] = "Le nom ou mot de passe est incorrecte."
        return render_template("connection.html", erreurs=erreurs)
    compte = Compte.query.filter_by(compte=request.form["nom"]).first()
    session["compte_id"] = compte.id
    session["connected"] = 1
    session["admin"] = compte.admin
    if session.get("redirect") is not None:
        url = session.pop("redirect")
        return redirect(f"/{url}")
    return redirect('/')


@comptes_routes.route('/deconnection')
def deconnection():
    """Déconnecte d'un compte"""
    session.pop("connected")
    session.pop("admin")
    session.pop("compte_id")
    return redirect('/')
